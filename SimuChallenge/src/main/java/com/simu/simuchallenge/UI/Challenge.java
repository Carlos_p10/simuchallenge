
package com.simu.simuchallenge.UI;

import com.simu.simuchallenge.main.TextPrompt;
import com.simu.simuchallenge.main.User;
import java.util.Arrays;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import javax.swing.JPanel;

/**
 *
 * @author carlo
 */
public class Challenge extends javax.swing.JFrame {    
    ImageIcon icon;    
    Icon iconimg;
    User user = new User();
    int clickCount = 0;
    int clickCountMesh = 0;    
    List<String> nameDom = Arrays.asList("Frente","Derecho", "Atras","Izquierdo","Arriba","Abajo");
    List<String> nameMesh = Arrays.asList("Original","Frente","Derecho", "Atras","Izquierdo","Arriba","Abajo");
    List<String> nameDomImg = Arrays.asList("/img/FrontTri.png","/img/RightTri.png", "/img/BackTri.png","/img/LeftTri.png","/img/UpTri.png","/img/DownTri.png");
    List<String> nameMeshImg = Arrays.asList("/img/Mesh.png","/img/FrontMesh.png","/img/RightMesh.png", "/img/BackMesh.png","/img/LeftMesh.png","/img/UpMesh.png","/img/DownMesh.png");
    List<String> stepMef = Arrays.asList("/img/paso1.png","/img/paso2.png","/img/paso3.png","/img/paso4.png","/img/paso5.png","/img/paso6.png");
    List<String> stepTit = Arrays.asList("Paso 1","Paso 2","Paso 3","Paso 4","Paso 5","Paso 6");
    List<String> stepDesc = Arrays.asList("Localización","Interpolación", " Aproximación del Modelo","Método de Residuos Ponderados","Método de Galerkin","Integración por Partes");
    
    public Challenge() {
        initComponents();
        getIcon();
//        jframe.setTitle("Aprende Rapido");
        TextPrompt txt = new TextPrompt("Nombre", jTextFieldName);
//        System.out.println("Bievenid@ " + this.nameUser);
    }
    
    private void userNa(String name){
        jLabelTitleDash.setText("Bienvenid@ " + name);
        jLabelTxtDom.setText("<html>"+name + " como podrás observar el modelo que escogí es una piramide para ver cada parte de este interactúa con los botones.</html>");
        jLabelTxtMesh.setText("<html>"+name + " si ya observaste el dominio, ahora esto te parece familiar solo que contiene un mallado de 9 nodos, interactúa con los botones de nuevo.</html>");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelBackAuth = new javax.swing.JPanel();
        jPanelFront = new javax.swing.JPanel();
        jLabelTitle = new javax.swing.JLabel();
        jLabelSubT = new javax.swing.JLabel();
        jTextFieldName = new javax.swing.JTextField();
        jButtonStart = new javax.swing.JButton();
        jPanelBackDash = new javax.swing.JPanel();
        jPanelFrontDash = new javax.swing.JPanel();
        jLabelTitleDash = new javax.swing.JLabel();
        jLabelText = new javax.swing.JLabel();
        jPanelDominio = new javax.swing.JPanel();
        jIImgDominio = new javax.swing.JLabel();
        jLabelDomini = new javax.swing.JLabel();
        jPanelMesh = new javax.swing.JPanel();
        jIImgMesh = new javax.swing.JLabel();
        jLabelMesh = new javax.swing.JLabel();
        jPanelAssambly = new javax.swing.JPanel();
        jIImgAssambly = new javax.swing.JLabel();
        jLabelAssambly = new javax.swing.JLabel();
        jPanelContorno = new javax.swing.JPanel();
        jIImgContorno = new javax.swing.JLabel();
        jLabelContorno = new javax.swing.JLabel();
        jPanelMef = new javax.swing.JPanel();
        jIImgMef = new javax.swing.JLabel();
        jLabelMef = new javax.swing.JLabel();
        jPanelBackDominio = new javax.swing.JPanel();
        jPanelTDom = new javax.swing.JPanel();
        jLabelTDom = new javax.swing.JLabel();
        jLabelTxtDom = new javax.swing.JLabel();
        jLabelImgDom = new javax.swing.JLabel();
        jLabelTiImgDom = new javax.swing.JLabel();
        jButtonPrevDom = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jButtonNextDom = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButtonReturnDom = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanelBackMesh = new javax.swing.JPanel();
        jPanelTMesh = new javax.swing.JPanel();
        jLabelTMesh = new javax.swing.JLabel();
        jLabelTxtMesh = new javax.swing.JLabel();
        jLabelImgMesh = new javax.swing.JLabel();
        jLabelTiImgMesh = new javax.swing.JLabel();
        jButtonPrevMesh = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jButtonNextMesh = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jButtonReturnMesh = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanelTableConec = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jPanelBackAssambly = new javax.swing.JPanel();
        jPanelTAssambly = new javax.swing.JPanel();
        jLabelTAssambly = new javax.swing.JLabel();
        jButtonReturnAssam = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabelImgAssam = new javax.swing.JLabel();
        jLabelImgAssamTa = new javax.swing.JLabel();
        jLabelAssamDesc = new javax.swing.JLabel();
        jPanelBackContorno = new javax.swing.JPanel();
        jPanelTContorno = new javax.swing.JPanel();
        jLabelTContorno = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jButtonReturnContorno = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabelImgContor = new javax.swing.JLabel();
        jLabelNeu = new javax.swing.JLabel();
        jLabelDri = new javax.swing.JLabel();
        jPanelsi = new javax.swing.JPanel();
        jLabelsi = new javax.swing.JLabel();
        jPanelno = new javax.swing.JPanel();
        jLabelno = new javax.swing.JLabel();
        jLabelAns = new javax.swing.JLabel();
        jPanelBackMef = new javax.swing.JPanel();
        jPanelTMef = new javax.swing.JPanel();
        jLabelTMef = new javax.swing.JLabel();
        jButtonReturnMef = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanelButtonModel = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanelButtonMef = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jPanelSubMefBack = new javax.swing.JPanel();
        jPanelModel = new javax.swing.JPanel();
        jLabelModelImg = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jPanelSubMef = new javax.swing.JPanel();
        jLabelImgMef = new javax.swing.JLabel();
        jPanelAnt = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jPanelSig = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabelMefDesc = new javax.swing.JLabel();
        jLabelMefTit = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Desafio");
        setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanelBackAuth.setBackground(new java.awt.Color(127, 183, 190));

        jPanelFront.setBackground(new java.awt.Color(83, 114, 117));

        jLabelTitle.setFont(new java.awt.Font("Monospaced", 1, 36)); // NOI18N
        jLabelTitle.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTitle.setText("Bienvenido");

        jLabelSubT.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        jLabelSubT.setForeground(new java.awt.Color(255, 255, 255));
        jLabelSubT.setText("<html>Para una experiencia mas personalizada ingrese su nombre abajo: </html>");
        jLabelSubT.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jLabelSubT.setPreferredSize(new java.awt.Dimension(649, 90));

        jTextFieldName.setBackground(new java.awt.Color(83, 114, 117));
        jTextFieldName.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        jTextFieldName.setForeground(new java.awt.Color(255, 255, 255));
        jTextFieldName.setToolTipText("");
        jTextFieldName.setBorder(null);

        jButtonStart.setBackground(new java.awt.Color(13, 19, 19));
        jButtonStart.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jButtonStart.setForeground(new java.awt.Color(255, 255, 255));
        jButtonStart.setText("Empezar");
        jButtonStart.setBorder(null);
        jButtonStart.setBorderPainted(false);
        jButtonStart.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButtonStartMousePressed(evt);
            }
        });
        jButtonStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonStartActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelFrontLayout = new javax.swing.GroupLayout(jPanelFront);
        jPanelFront.setLayout(jPanelFrontLayout);
        jPanelFrontLayout.setHorizontalGroup(
            jPanelFrontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFrontLayout.createSequentialGroup()
                .addContainerGap(100, Short.MAX_VALUE)
                .addGroup(jPanelFrontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanelFrontLayout.createSequentialGroup()
                        .addComponent(jTextFieldName, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 103, Short.MAX_VALUE)
                        .addComponent(jButtonStart, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabelSubT, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jLabelTitle, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(92, 92, 92))
        );
        jPanelFrontLayout.setVerticalGroup(
            jPanelFrontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelFrontLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jLabelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelSubT, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addGroup(jPanelFrontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldName, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonStart, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(58, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelBackAuthLayout = new javax.swing.GroupLayout(jPanelBackAuth);
        jPanelBackAuth.setLayout(jPanelBackAuthLayout);
        jPanelBackAuthLayout.setHorizontalGroup(
            jPanelBackAuthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBackAuthLayout.createSequentialGroup()
                .addGap(114, 114, 114)
                .addComponent(jPanelFront, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(131, Short.MAX_VALUE))
        );
        jPanelBackAuthLayout.setVerticalGroup(
            jPanelBackAuthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBackAuthLayout.createSequentialGroup()
                .addGap(112, 112, 112)
                .addComponent(jPanelFront, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(168, Short.MAX_VALUE))
        );

        getContentPane().add(jPanelBackAuth, "card2");

        jPanelBackDash.setBackground(new java.awt.Color(120, 138, 163));
        jPanelBackDash.setForeground(new java.awt.Color(120, 138, 163));
        jPanelBackDash.setPreferredSize(new java.awt.Dimension(820, 510));

        jPanelFrontDash.setBackground(new java.awt.Color(127, 183, 190));

        jLabelTitleDash.setFont(new java.awt.Font("Monospaced", 1, 36)); // NOI18N

        jLabelText.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabelText.setText("<html>En cada opción habrá una breve explicación para entender el proceso de cada una de ellas. </html>");

        javax.swing.GroupLayout jPanelFrontDashLayout = new javax.swing.GroupLayout(jPanelFrontDash);
        jPanelFrontDash.setLayout(jPanelFrontDashLayout);
        jPanelFrontDashLayout.setHorizontalGroup(
            jPanelFrontDashLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelFrontDashLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(jPanelFrontDashLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelFrontDashLayout.createSequentialGroup()
                        .addComponent(jLabelText, javax.swing.GroupLayout.PREFERRED_SIZE, 536, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanelFrontDashLayout.createSequentialGroup()
                        .addComponent(jLabelTitleDash, javax.swing.GroupLayout.DEFAULT_SIZE, 383, Short.MAX_VALUE)
                        .addGap(387, 387, 387))))
        );
        jPanelFrontDashLayout.setVerticalGroup(
            jPanelFrontDashLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelFrontDashLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabelTitleDash, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jLabelText, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        jPanelDominio.setBackground(new java.awt.Color(127, 183, 190));
        jPanelDominio.setPreferredSize(new java.awt.Dimension(120, 120));
        jPanelDominio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanelDominioMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanelDominioMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanelDominioMousePressed(evt);
            }
        });

        jIImgDominio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dashboard/icons8_orthogonal_view_60px.png"))); // NOI18N
        jIImgDominio.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelDomini.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelDomini.setText("Dominio");

        javax.swing.GroupLayout jPanelDominioLayout = new javax.swing.GroupLayout(jPanelDominio);
        jPanelDominio.setLayout(jPanelDominioLayout);
        jPanelDominioLayout.setHorizontalGroup(
            jPanelDominioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelDominioLayout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addGroup(jPanelDominioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jIImgDominio)
                    .addGroup(jPanelDominioLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabelDomini)))
                .addGap(30, 30, 30))
        );
        jPanelDominioLayout.setVerticalGroup(
            jPanelDominioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelDominioLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jIImgDominio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelDomini)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        jPanelMesh.setBackground(new java.awt.Color(127, 183, 190));
        jPanelMesh.setPreferredSize(new java.awt.Dimension(120, 120));
        jPanelMesh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanelMeshMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanelMeshMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanelMeshMousePressed(evt);
            }
        });

        jIImgMesh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dashboard/icons8_mesh_80px_1.png"))); // NOI18N
        jIImgMesh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelMesh.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelMesh.setText("Malla");

        javax.swing.GroupLayout jPanelMeshLayout = new javax.swing.GroupLayout(jPanelMesh);
        jPanelMesh.setLayout(jPanelMeshLayout);
        jPanelMeshLayout.setHorizontalGroup(
            jPanelMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMeshLayout.createSequentialGroup()
                .addContainerGap(21, Short.MAX_VALUE)
                .addGroup(jPanelMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelMeshLayout.createSequentialGroup()
                        .addComponent(jIImgMesh, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(19, 19, 19))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelMeshLayout.createSequentialGroup()
                        .addComponent(jLabelMesh)
                        .addGap(43, 43, 43))))
        );
        jPanelMeshLayout.setVerticalGroup(
            jPanelMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMeshLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jIImgMesh, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addComponent(jLabelMesh)
                .addContainerGap())
        );

        jPanelAssambly.setBackground(new java.awt.Color(127, 183, 190));
        jPanelAssambly.setPreferredSize(new java.awt.Dimension(120, 120));
        jPanelAssambly.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanelAssamblyMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanelAssamblyMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanelAssamblyMousePressed(evt);
            }
        });

        jIImgAssambly.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dashboard/icons8_puzzle_48px.png"))); // NOI18N
        jIImgAssambly.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jIImgAssambly.setMaximumSize(new java.awt.Dimension(60, 60));
        jIImgAssambly.setMinimumSize(new java.awt.Dimension(60, 60));
        jIImgAssambly.setPreferredSize(new java.awt.Dimension(60, 60));

        jLabelAssambly.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelAssambly.setText("Ensamblaje");

        javax.swing.GroupLayout jPanelAssamblyLayout = new javax.swing.GroupLayout(jPanelAssambly);
        jPanelAssambly.setLayout(jPanelAssamblyLayout);
        jPanelAssamblyLayout.setHorizontalGroup(
            jPanelAssamblyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelAssamblyLayout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addGroup(jPanelAssamblyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelAssambly)
                    .addComponent(jIImgAssambly, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22))
        );
        jPanelAssamblyLayout.setVerticalGroup(
            jPanelAssamblyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAssamblyLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jIImgAssambly, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelAssambly)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        jPanelContorno.setBackground(new java.awt.Color(127, 183, 190));
        jPanelContorno.setPreferredSize(new java.awt.Dimension(120, 120));
        jPanelContorno.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanelContornoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanelContornoMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanelContornoMousePressed(evt);
            }
        });

        jIImgContorno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dashboard/icons8_front_view_64px.png"))); // NOI18N
        jIImgContorno.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelContorno.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelContorno.setText("C.Contorno");

        javax.swing.GroupLayout jPanelContornoLayout = new javax.swing.GroupLayout(jPanelContorno);
        jPanelContorno.setLayout(jPanelContornoLayout);
        jPanelContornoLayout.setHorizontalGroup(
            jPanelContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelContornoLayout.createSequentialGroup()
                .addContainerGap(27, Short.MAX_VALUE)
                .addGroup(jPanelContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelContorno)
                    .addComponent(jIImgContorno))
                .addGap(22, 22, 22))
        );
        jPanelContornoLayout.setVerticalGroup(
            jPanelContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelContornoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jIImgContorno)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelContorno)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jPanelMef.setBackground(new java.awt.Color(127, 183, 190));
        jPanelMef.setPreferredSize(new java.awt.Dimension(120, 120));
        jPanelMef.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanelMefMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanelMefMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanelMefMousePressed(evt);
            }
        });

        jIImgMef.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dashboard/icons8_table_52px.png"))); // NOI18N
        jIImgMef.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelMef.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelMef.setText("Modelo y MEF");

        javax.swing.GroupLayout jPanelMefLayout = new javax.swing.GroupLayout(jPanelMef);
        jPanelMef.setLayout(jPanelMefLayout);
        jPanelMefLayout.setHorizontalGroup(
            jPanelMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMefLayout.createSequentialGroup()
                .addGroup(jPanelMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelMefLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabelMef))
                    .addGroup(jPanelMefLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jIImgMef)))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jPanelMefLayout.setVerticalGroup(
            jPanelMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMefLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jIImgMef)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelMef)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelBackDashLayout = new javax.swing.GroupLayout(jPanelBackDash);
        jPanelBackDash.setLayout(jPanelBackDashLayout);
        jPanelBackDashLayout.setHorizontalGroup(
            jPanelBackDashLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelFrontDash, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanelBackDashLayout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addComponent(jPanelDominio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(jPanelAssambly, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanelMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(jPanelContorno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanelMef, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelBackDashLayout.setVerticalGroup(
            jPanelBackDashLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBackDashLayout.createSequentialGroup()
                .addComponent(jPanelFrontDash, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(79, 79, 79)
                .addGroup(jPanelBackDashLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelBackDashLayout.createSequentialGroup()
                        .addGroup(jPanelBackDashLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanelDominio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanelMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanelMef, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBackDashLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanelBackDashLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanelContorno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanelAssambly, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(120, Short.MAX_VALUE))
        );

        getContentPane().add(jPanelBackDash, "card3");

        jPanelBackDominio.setBackground(new java.awt.Color(37, 48, 49));

        jPanelTDom.setBackground(new java.awt.Color(107, 15, 26));

        jLabelTDom.setBackground(new java.awt.Color(107, 15, 26));
        jLabelTDom.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabelTDom.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTDom.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dashboard/cube_white.png"))); // NOI18N
        jLabelTDom.setText(" Dominio");

        jLabelTxtDom.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabelTxtDom.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanelTDomLayout = new javax.swing.GroupLayout(jPanelTDom);
        jPanelTDom.setLayout(jPanelTDomLayout);
        jPanelTDomLayout.setHorizontalGroup(
            jPanelTDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTDomLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(jPanelTDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelTxtDom, javax.swing.GroupLayout.PREFERRED_SIZE, 641, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelTDom, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(142, Short.MAX_VALUE))
        );
        jPanelTDomLayout.setVerticalGroup(
            jPanelTDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTDomLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabelTDom, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelTxtDom, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabelImgDom.setPreferredSize(new java.awt.Dimension(300, 250));

        jLabelTiImgDom.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabelTiImgDom.setForeground(new java.awt.Color(255, 255, 255));

        jButtonPrevDom.setBackground(new java.awt.Color(107, 15, 26));
        jButtonPrevDom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonPrevDomMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonPrevDomMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButtonPrevDomMousePressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Anterior");

        javax.swing.GroupLayout jButtonPrevDomLayout = new javax.swing.GroupLayout(jButtonPrevDom);
        jButtonPrevDom.setLayout(jButtonPrevDomLayout);
        jButtonPrevDomLayout.setHorizontalGroup(
            jButtonPrevDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jButtonPrevDomLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jButtonPrevDomLayout.setVerticalGroup(
            jButtonPrevDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jButtonPrevDomLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jButtonNextDom.setBackground(new java.awt.Color(107, 15, 26));
        jButtonNextDom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonNextDomMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonNextDomMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonNextDomMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButtonNextDomMousePressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Siguiente");

        javax.swing.GroupLayout jButtonNextDomLayout = new javax.swing.GroupLayout(jButtonNextDom);
        jButtonNextDom.setLayout(jButtonNextDomLayout);
        jButtonNextDomLayout.setHorizontalGroup(
            jButtonNextDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jButtonNextDomLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jButtonNextDomLayout.setVerticalGroup(
            jButtonNextDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jButtonNextDomLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jButtonReturnDom.setBackground(new java.awt.Color(107, 15, 26));
        jButtonReturnDom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonReturnDomMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonReturnDomMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButtonReturnDomMousePressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Regresar");

        javax.swing.GroupLayout jButtonReturnDomLayout = new javax.swing.GroupLayout(jButtonReturnDom);
        jButtonReturnDom.setLayout(jButtonReturnDomLayout);
        jButtonReturnDomLayout.setHorizontalGroup(
            jButtonReturnDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jButtonReturnDomLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jButtonReturnDomLayout.setVerticalGroup(
            jButtonReturnDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jButtonReturnDomLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelBackDominioLayout = new javax.swing.GroupLayout(jPanelBackDominio);
        jPanelBackDominio.setLayout(jPanelBackDominioLayout);
        jPanelBackDominioLayout.setHorizontalGroup(
            jPanelBackDominioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelTDom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanelBackDominioLayout.createSequentialGroup()
                .addGroup(jPanelBackDominioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelBackDominioLayout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addComponent(jButtonPrevDom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabelImgDom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonNextDom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelBackDominioLayout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(jButtonReturnDom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelBackDominioLayout.createSequentialGroup()
                        .addGap(337, 337, 337)
                        .addComponent(jLabelTiImgDom, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelBackDominioLayout.setVerticalGroup(
            jPanelBackDominioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBackDominioLayout.createSequentialGroup()
                .addComponent(jPanelTDom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabelTiImgDom, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanelBackDominioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelImgDom, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanelBackDominioLayout.createSequentialGroup()
                        .addGap(79, 79, 79)
                        .addComponent(jButtonPrevDom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelBackDominioLayout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addComponent(jButtonNextDom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 83, Short.MAX_VALUE)
                .addComponent(jButtonReturnDom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        getContentPane().add(jPanelBackDominio, "card4");

        jPanelBackMesh.setBackground(new java.awt.Color(37, 48, 49));

        jPanelTMesh.setBackground(new java.awt.Color(107, 15, 26));

        jLabelTMesh.setBackground(new java.awt.Color(107, 15, 26));
        jLabelTMesh.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabelTMesh.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTMesh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dashboard/mesh_white.png"))); // NOI18N
        jLabelTMesh.setText(" Malla");

        jLabelTxtMesh.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabelTxtMesh.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanelTMeshLayout = new javax.swing.GroupLayout(jPanelTMesh);
        jPanelTMesh.setLayout(jPanelTMeshLayout);
        jPanelTMeshLayout.setHorizontalGroup(
            jPanelTMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTMeshLayout.createSequentialGroup()
                .addGroup(jPanelTMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelTMeshLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(jLabelTMesh, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelTMeshLayout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(jLabelTxtMesh, javax.swing.GroupLayout.PREFERRED_SIZE, 641, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(124, Short.MAX_VALUE))
        );
        jPanelTMeshLayout.setVerticalGroup(
            jPanelTMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTMeshLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabelTMesh, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelTxtMesh, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabelImgMesh.setPreferredSize(new java.awt.Dimension(300, 280));

        jLabelTiImgMesh.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabelTiImgMesh.setForeground(new java.awt.Color(255, 255, 255));

        jButtonPrevMesh.setBackground(new java.awt.Color(107, 15, 26));
        jButtonPrevMesh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonPrevMeshMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonPrevMeshMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButtonPrevMeshMousePressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Anterior");

        javax.swing.GroupLayout jButtonPrevMeshLayout = new javax.swing.GroupLayout(jButtonPrevMesh);
        jButtonPrevMesh.setLayout(jButtonPrevMeshLayout);
        jButtonPrevMeshLayout.setHorizontalGroup(
            jButtonPrevMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jButtonPrevMeshLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jButtonPrevMeshLayout.setVerticalGroup(
            jButtonPrevMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jButtonPrevMeshLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jButtonNextMesh.setBackground(new java.awt.Color(107, 15, 26));
        jButtonNextMesh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonNextMeshMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonNextMeshMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonNextMeshMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButtonNextMeshMousePressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Siguiente");

        javax.swing.GroupLayout jButtonNextMeshLayout = new javax.swing.GroupLayout(jButtonNextMesh);
        jButtonNextMesh.setLayout(jButtonNextMeshLayout);
        jButtonNextMeshLayout.setHorizontalGroup(
            jButtonNextMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jButtonNextMeshLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jButtonNextMeshLayout.setVerticalGroup(
            jButtonNextMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jButtonNextMeshLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jButtonReturnMesh.setBackground(new java.awt.Color(107, 15, 26));
        jButtonReturnMesh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonReturnMeshMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonReturnMeshMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButtonReturnMeshMousePressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Regresar");

        javax.swing.GroupLayout jButtonReturnMeshLayout = new javax.swing.GroupLayout(jButtonReturnMesh);
        jButtonReturnMesh.setLayout(jButtonReturnMeshLayout);
        jButtonReturnMeshLayout.setHorizontalGroup(
            jButtonReturnMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jButtonReturnMeshLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jButtonReturnMeshLayout.setVerticalGroup(
            jButtonReturnMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jButtonReturnMeshLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanelTableConec.setBackground(new java.awt.Color(107, 15, 26));
        jPanelTableConec.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanelTableConecMousePressed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Ver Tabla de Conectividades");

        javax.swing.GroupLayout jPanelTableConecLayout = new javax.swing.GroupLayout(jPanelTableConec);
        jPanelTableConec.setLayout(jPanelTableConecLayout);
        jPanelTableConecLayout.setHorizontalGroup(
            jPanelTableConecLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTableConecLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanelTableConecLayout.setVerticalGroup(
            jPanelTableConecLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTableConecLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelBackMeshLayout = new javax.swing.GroupLayout(jPanelBackMesh);
        jPanelBackMesh.setLayout(jPanelBackMeshLayout);
        jPanelBackMeshLayout.setHorizontalGroup(
            jPanelBackMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelTMesh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBackMeshLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelBackMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBackMeshLayout.createSequentialGroup()
                        .addComponent(jLabelTiImgMesh, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(333, 333, 333))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBackMeshLayout.createSequentialGroup()
                        .addComponent(jButtonPrevMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabelImgMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanelBackMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelBackMeshLayout.createSequentialGroup()
                                .addComponent(jPanelTableConec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBackMeshLayout.createSequentialGroup()
                                .addComponent(jButtonNextMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(170, 170, 170))))))
            .addGroup(jPanelBackMeshLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(jButtonReturnMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanelBackMeshLayout.setVerticalGroup(
            jPanelBackMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBackMeshLayout.createSequentialGroup()
                .addComponent(jPanelTMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(jLabelTiImgMesh, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanelBackMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelBackMeshLayout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addComponent(jButtonPrevMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelBackMeshLayout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(jButtonNextMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelBackMeshLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelImgMesh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelBackMeshLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanelTableConec, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonReturnMesh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(33, 33, 33))
        );

        getContentPane().add(jPanelBackMesh, "card4");

        jPanelBackAssambly.setBackground(new java.awt.Color(37, 48, 49));

        jPanelTAssambly.setBackground(new java.awt.Color(107, 15, 26));

        jLabelTAssambly.setBackground(new java.awt.Color(107, 15, 26));
        jLabelTAssambly.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabelTAssambly.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTAssambly.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dashboard/puzzle_white.png"))); // NOI18N
        jLabelTAssambly.setText(" Ensamblaje");

        javax.swing.GroupLayout jPanelTAssamblyLayout = new javax.swing.GroupLayout(jPanelTAssambly);
        jPanelTAssambly.setLayout(jPanelTAssamblyLayout);
        jPanelTAssamblyLayout.setHorizontalGroup(
            jPanelTAssamblyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTAssamblyLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jLabelTAssambly, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(589, Short.MAX_VALUE))
        );
        jPanelTAssamblyLayout.setVerticalGroup(
            jPanelTAssamblyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTAssamblyLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabelTAssambly, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(49, Short.MAX_VALUE))
        );

        jButtonReturnAssam.setBackground(new java.awt.Color(107, 15, 26));
        jButtonReturnAssam.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonReturnAssamMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonReturnAssamMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButtonReturnAssamMousePressed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("Regresar");

        javax.swing.GroupLayout jButtonReturnAssamLayout = new javax.swing.GroupLayout(jButtonReturnAssam);
        jButtonReturnAssam.setLayout(jButtonReturnAssamLayout);
        jButtonReturnAssamLayout.setHorizontalGroup(
            jButtonReturnAssamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jButtonReturnAssamLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel16)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jButtonReturnAssamLayout.setVerticalGroup(
            jButtonReturnAssamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jButtonReturnAssamLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabelAssamDesc.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        jLabelAssamDesc.setForeground(new java.awt.Color(255, 255, 255));
        jLabelAssamDesc.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelAssamDesc.setText("<html>Este paso resulta un poco engorroso trabajarlo en tablas en Excel pero lo resumiremos explicando el primer elemento, la imagen de abajo representa el elemento y la posición de los nodos de tabla de conectividades, en la parte del MEF observamos que nos queda una matriz de M_(8x8) X_(8x1) = b_(8x1) eso lo podemos ver reflejado en la imagen de arriba, este proceso hay que repetirlo para los 8 elementos de la misma tabla y al final unirlas en una sola donde se sumaran aquellas posiciones que coincidan en filas y columnas, al final el ensamblaje nos que da una matriz de 18x18.</html>");

        javax.swing.GroupLayout jPanelBackAssamblyLayout = new javax.swing.GroupLayout(jPanelBackAssambly);
        jPanelBackAssambly.setLayout(jPanelBackAssamblyLayout);
        jPanelBackAssamblyLayout.setHorizontalGroup(
            jPanelBackAssamblyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelTAssambly, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBackAssamblyLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelBackAssamblyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBackAssamblyLayout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(jLabelAssamDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 385, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelImgAssam, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBackAssamblyLayout.createSequentialGroup()
                        .addComponent(jButtonReturnAssam, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelImgAssamTa, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(22, 22, 22))
        );
        jPanelBackAssamblyLayout.setVerticalGroup(
            jPanelBackAssamblyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBackAssamblyLayout.createSequentialGroup()
                .addComponent(jPanelTAssambly, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanelBackAssamblyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanelBackAssamblyLayout.createSequentialGroup()
                        .addComponent(jLabelImgAssam, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabelImgAssamTa, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelBackAssamblyLayout.createSequentialGroup()
                        .addComponent(jLabelAssamDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(jButtonReturnAssam, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(41, Short.MAX_VALUE))
        );

        getContentPane().add(jPanelBackAssambly, "card4");

        jPanelBackContorno.setBackground(new java.awt.Color(37, 48, 49));

        jPanelTContorno.setBackground(new java.awt.Color(107, 15, 26));

        jLabelTContorno.setBackground(new java.awt.Color(107, 15, 26));
        jLabelTContorno.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabelTContorno.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTContorno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dashboard/front_white.png"))); // NOI18N
        jLabelTContorno.setText(" Contorno");

        jLabel14.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("<html>Para esta ultima parte necesitare de tu ayuda para verificar si los datos que te diré a continuación son correctos o no, para ello responderás con los botones de abajo que dicen si o no, por favor lee las indicaciones antes.</html>");

        javax.swing.GroupLayout jPanelTContornoLayout = new javax.swing.GroupLayout(jPanelTContorno);
        jPanelTContorno.setLayout(jPanelTContornoLayout);
        jPanelTContornoLayout.setHorizontalGroup(
            jPanelTContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTContornoLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jLabelTContorno, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(85, 85, 85)
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelTContornoLayout.setVerticalGroup(
            jPanelTContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTContornoLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanelTContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabelTContorno, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        jButtonReturnContorno.setBackground(new java.awt.Color(107, 15, 26));
        jButtonReturnContorno.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonReturnContornoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonReturnContornoMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButtonReturnContornoMousePressed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("Regresar");

        javax.swing.GroupLayout jButtonReturnContornoLayout = new javax.swing.GroupLayout(jButtonReturnContorno);
        jButtonReturnContorno.setLayout(jButtonReturnContornoLayout);
        jButtonReturnContornoLayout.setHorizontalGroup(
            jButtonReturnContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jButtonReturnContornoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel15)
                .addContainerGap())
        );
        jButtonReturnContornoLayout.setVerticalGroup(
            jButtonReturnContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jButtonReturnContornoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel17.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("<html>Para resolver el problema se necesitan establecer las variables de contorno. Acá se aplican 2 la superficie amarilla es la condición de Drichlet y la negra es la condición de Neumann, ahora contesta, las respuestas proporcionadas abajo contienen los nodos correctos?</html>");

        jLabelImgContor.setPreferredSize(new java.awt.Dimension(450, 450));

        jLabelNeu.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jLabelNeu.setForeground(new java.awt.Color(255, 255, 255));
        jLabelNeu.setText("<html>Nuemann: [8,1,6]</html>");

        jLabelDri.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jLabelDri.setForeground(new java.awt.Color(255, 255, 255));
        jLabelDri.setText("<html>Drichlet: [1,5,8]</html>");

        jPanelsi.setBackground(new java.awt.Color(107, 15, 26));
        jPanelsi.setToolTipText("");
        jPanelsi.setName(""); // NOI18N
        jPanelsi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanelsiMousePressed(evt);
            }
        });

        jLabelsi.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabelsi.setForeground(new java.awt.Color(255, 255, 255));
        jLabelsi.setText("Si");

        javax.swing.GroupLayout jPanelsiLayout = new javax.swing.GroupLayout(jPanelsi);
        jPanelsi.setLayout(jPanelsiLayout);
        jPanelsiLayout.setHorizontalGroup(
            jPanelsiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelsiLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelsi)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelsiLayout.setVerticalGroup(
            jPanelsiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelsiLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelsi)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanelno.setBackground(new java.awt.Color(107, 15, 26));
        jPanelno.setToolTipText("");
        jPanelno.setName(""); // NOI18N
        jPanelno.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanelnoMousePressed(evt);
            }
        });

        jLabelno.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        jLabelno.setForeground(new java.awt.Color(255, 255, 255));
        jLabelno.setText("No");

        javax.swing.GroupLayout jPanelnoLayout = new javax.swing.GroupLayout(jPanelno);
        jPanelno.setLayout(jPanelnoLayout);
        jPanelnoLayout.setHorizontalGroup(
            jPanelnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelnoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelno)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelnoLayout.setVerticalGroup(
            jPanelnoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelnoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelno)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabelAns.setFont(new java.awt.Font("Monospaced", 0, 16)); // NOI18N
        jLabelAns.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanelBackContornoLayout = new javax.swing.GroupLayout(jPanelBackContorno);
        jPanelBackContorno.setLayout(jPanelBackContornoLayout);
        jPanelBackContornoLayout.setHorizontalGroup(
            jPanelBackContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelTContorno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanelBackContornoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelImgContor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanelBackContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBackContornoLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonReturnContorno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(56, 56, 56))
                    .addGroup(jPanelBackContornoLayout.createSequentialGroup()
                        .addGroup(jPanelBackContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelBackContornoLayout.createSequentialGroup()
                                .addGap(64, 64, 64)
                                .addGroup(jPanelBackContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanelBackContornoLayout.createSequentialGroup()
                                        .addComponent(jLabelNeu, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabelDri, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanelBackContornoLayout.createSequentialGroup()
                                        .addGap(28, 28, 28)
                                        .addComponent(jPanelsi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(64, 64, 64)
                                        .addComponent(jPanelno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanelBackContornoLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelAns, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanelBackContornoLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(19, Short.MAX_VALUE))))
        );
        jPanelBackContornoLayout.setVerticalGroup(
            jPanelBackContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBackContornoLayout.createSequentialGroup()
                .addComponent(jPanelTContorno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelBackContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelBackContornoLayout.createSequentialGroup()
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelBackContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabelNeu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelDri, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelBackContornoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanelsi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanelno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jLabelAns, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonReturnContorno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanelBackContornoLayout.createSequentialGroup()
                        .addComponent(jLabelImgContor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        getContentPane().add(jPanelBackContorno, "card4");

        jPanelBackMef.setBackground(new java.awt.Color(37, 48, 49));
        jPanelBackMef.setPreferredSize(new java.awt.Dimension(820, 620));

        jPanelTMef.setBackground(new java.awt.Color(107, 15, 26));

        jLabelTMef.setBackground(new java.awt.Color(107, 15, 26));
        jLabelTMef.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jLabelTMef.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTMef.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dashboard/table_white.png"))); // NOI18N
        jLabelTMef.setText(" Modelo y MEF ");

        jButtonReturnMef.setBackground(new java.awt.Color(37, 48, 49));
        jButtonReturnMef.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonReturnMefMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonReturnMefMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButtonReturnMefMousePressed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Monospaced", 0, 16)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Regresar");

        javax.swing.GroupLayout jButtonReturnMefLayout = new javax.swing.GroupLayout(jButtonReturnMef);
        jButtonReturnMef.setLayout(jButtonReturnMefLayout);
        jButtonReturnMefLayout.setHorizontalGroup(
            jButtonReturnMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jButtonReturnMefLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel7)
                .addContainerGap())
        );
        jButtonReturnMefLayout.setVerticalGroup(
            jButtonReturnMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jButtonReturnMefLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanelButtonModel.setBackground(new java.awt.Color(223, 204, 206));
        jPanelButtonModel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanelButtonModelMousePressed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        jLabel8.setText("Modelo");

        javax.swing.GroupLayout jPanelButtonModelLayout = new javax.swing.GroupLayout(jPanelButtonModel);
        jPanelButtonModel.setLayout(jPanelButtonModelLayout);
        jPanelButtonModelLayout.setHorizontalGroup(
            jPanelButtonModelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelButtonModelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelButtonModelLayout.setVerticalGroup(
            jPanelButtonModelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelButtonModelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanelButtonMef.setBackground(new java.awt.Color(223, 204, 206));
        jPanelButtonMef.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanelButtonMefMousePressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        jLabel9.setText("MEF");

        javax.swing.GroupLayout jPanelButtonMefLayout = new javax.swing.GroupLayout(jPanelButtonMef);
        jPanelButtonMef.setLayout(jPanelButtonMefLayout);
        jPanelButtonMefLayout.setHorizontalGroup(
            jPanelButtonMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelButtonMefLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelButtonMefLayout.setVerticalGroup(
            jPanelButtonMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelButtonMefLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelTMefLayout = new javax.swing.GroupLayout(jPanelTMef);
        jPanelTMef.setLayout(jPanelTMefLayout);
        jPanelTMefLayout.setHorizontalGroup(
            jPanelTMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTMefLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabelTMef))
            .addGroup(jPanelTMefLayout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jButtonReturnMef, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanelTMefLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelTMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelButtonModel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelButtonMef, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanelTMefLayout.setVerticalGroup(
            jPanelTMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTMefLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabelTMef, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(65, 65, 65)
                .addComponent(jPanelButtonModel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(jPanelButtonMef, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonReturnMef, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );

        jPanelSubMefBack.setBackground(new java.awt.Color(37, 48, 49));
        jPanelSubMefBack.setLayout(new java.awt.CardLayout());

        jPanelModel.setBackground(new java.awt.Color(37, 48, 49));

        jLabelModelImg.setPreferredSize(new java.awt.Dimension(350, 200));

        jLabel11.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("Modelo");

        jLabel18.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel18.setText("<html>Si ya conociste el dominio sobre el cual se trabajara, ahora se establece el modelo al que se le aplicara el MEF, el modelo tiene los coeficientes x y y estas influyen en la definición de matrices en lo que te mostrare, no influyen pero es un dato importante, y también tiene las incógnitas A y B.</html>");

        javax.swing.GroupLayout jPanelModelLayout = new javax.swing.GroupLayout(jPanelModel);
        jPanelModel.setLayout(jPanelModelLayout);
        jPanelModelLayout.setHorizontalGroup(
            jPanelModelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelModelLayout.createSequentialGroup()
                .addGroup(jPanelModelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelModelLayout.createSequentialGroup()
                        .addGap(188, 188, 188)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelModelLayout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addGroup(jPanelModelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelModelImg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 364, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(98, Short.MAX_VALUE))
        );
        jPanelModelLayout.setVerticalGroup(
            jPanelModelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelModelLayout.createSequentialGroup()
                .addGap(71, 71, 71)
                .addComponent(jLabelModelImg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(77, Short.MAX_VALUE))
        );

        jPanelSubMefBack.add(jPanelModel, "card2");

        jPanelSubMef.setBackground(new java.awt.Color(37, 48, 49));

        jLabelImgMef.setPreferredSize(new java.awt.Dimension(400, 400));

        jPanelAnt.setBackground(new java.awt.Color(107, 15, 26));
        jPanelAnt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanelAntMousePressed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Monospaced", 0, 16)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Anterior");

        javax.swing.GroupLayout jPanelAntLayout = new javax.swing.GroupLayout(jPanelAnt);
        jPanelAnt.setLayout(jPanelAntLayout);
        jPanelAntLayout.setHorizontalGroup(
            jPanelAntLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAntLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanelAntLayout.setVerticalGroup(
            jPanelAntLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAntLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanelSig.setBackground(new java.awt.Color(107, 15, 26));
        jPanelSig.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanelSigMousePressed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Monospaced", 0, 15)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Siguiente");

        javax.swing.GroupLayout jPanelSigLayout = new javax.swing.GroupLayout(jPanelSig);
        jPanelSig.setLayout(jPanelSigLayout);
        jPanelSigLayout.setHorizontalGroup(
            jPanelSigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelSigLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanelSigLayout.setVerticalGroup(
            jPanelSigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelSigLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabelMefDesc.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jLabelMefDesc.setForeground(new java.awt.Color(255, 255, 255));

        jLabelMefTit.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jLabelMefTit.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanelSubMefLayout = new javax.swing.GroupLayout(jPanelSubMef);
        jPanelSubMef.setLayout(jPanelSubMefLayout);
        jPanelSubMefLayout.setHorizontalGroup(
            jPanelSubMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelSubMefLayout.createSequentialGroup()
                .addGroup(jPanelSubMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelSubMefLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabelMefDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 479, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelSubMefLayout.createSequentialGroup()
                        .addGap(80, 80, 80)
                        .addGroup(jPanelSubMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabelImgMef, javax.swing.GroupLayout.PREFERRED_SIZE, 409, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelSubMefLayout.createSequentialGroup()
                                .addComponent(jPanelAnt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanelSig, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanelSubMefLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabelMefTit, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(75, Short.MAX_VALUE))
        );
        jPanelSubMefLayout.setVerticalGroup(
            jPanelSubMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelSubMefLayout.createSequentialGroup()
                .addComponent(jLabelMefTit, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelMefDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelImgMef, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelSubMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelSig, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanelAnt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(43, Short.MAX_VALUE))
        );

        jPanelSubMefBack.add(jPanelSubMef, "card3");

        javax.swing.GroupLayout jPanelBackMefLayout = new javax.swing.GroupLayout(jPanelBackMef);
        jPanelBackMef.setLayout(jPanelBackMefLayout);
        jPanelBackMefLayout.setHorizontalGroup(
            jPanelBackMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBackMefLayout.createSequentialGroup()
                .addComponent(jPanelTMef, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanelSubMefBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanelBackMefLayout.setVerticalGroup(
            jPanelBackMefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelTMef, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanelBackMefLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelSubMefBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanelBackMef, "card4");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonStartMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonStartMousePressed
        jPanelBackAuth.setVisible(false);
        jPanelBackDash.setVisible(true);        
        user.setUserName(jTextFieldName.getText()); 
        userNa(user.getUserName());        
    }//GEN-LAST:event_jButtonStartMousePressed


    private void jPanelDominioMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelDominioMouseEntered
        setColor(jPanelDominio);
    }//GEN-LAST:event_jPanelDominioMouseEntered

    private void jPanelDominioMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelDominioMouseExited
        resetColor(jPanelDominio);
    }//GEN-LAST:event_jPanelDominioMouseExited

    private void jPanelDominioMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelDominioMousePressed
        jPanelBackDash.setVisible(false);
        jPanelBackDominio.setVisible(true);
        jLabelTiImgDom.setText(nameDom.get(0));
        jLabelImgDom.setIcon(new ImageIcon(getClass().getResource(nameDomImg.get(0))));
    }//GEN-LAST:event_jPanelDominioMousePressed

    private void jPanelMeshMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelMeshMouseEntered
        setColor(jPanelMesh);
    }//GEN-LAST:event_jPanelMeshMouseEntered

    private void jPanelMeshMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelMeshMouseExited
        resetColor(jPanelMesh);
    }//GEN-LAST:event_jPanelMeshMouseExited

    private void jPanelMeshMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelMeshMousePressed
        jPanelBackDash.setVisible(false);
        jPanelBackMesh.setVisible(true);
        jLabelTiImgMesh.setText(nameMesh.get(0));
        jLabelImgMesh.setIcon(new ImageIcon(getClass().getResource(nameMeshImg.get(0))));
    }//GEN-LAST:event_jPanelMeshMousePressed

    private void jPanelAssamblyMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelAssamblyMouseEntered
        setColor(jPanelAssambly);
    }//GEN-LAST:event_jPanelAssamblyMouseEntered

    private void jPanelAssamblyMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelAssamblyMouseExited
        resetColor(jPanelAssambly);
    }//GEN-LAST:event_jPanelAssamblyMouseExited

    private void jPanelAssamblyMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelAssamblyMousePressed
        jPanelBackDash.setVisible(false);
        jPanelBackAssambly.setVisible(true);
        jLabelImgAssam.setIcon(new ImageIcon(getClass().getResource("/img/ensam.png")));
        jLabelImgAssamTa.setIcon(new ImageIcon(getClass().getResource("/img/tabla1.png")));
        //setIcon(new javax.swing.ImageIcon(getClass().getResource(
    }//GEN-LAST:event_jPanelAssamblyMousePressed

    private void jPanelContornoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelContornoMouseEntered
        setColor(jPanelContorno);
    }//GEN-LAST:event_jPanelContornoMouseEntered

    private void jPanelContornoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelContornoMouseExited
        resetColor(jPanelContorno);
    }//GEN-LAST:event_jPanelContornoMouseExited

    private void jPanelContornoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelContornoMousePressed
        jPanelBackDash.setVisible(false);
        jPanelBackContorno.setVisible(true);
        jLabelImgContor.setIcon(new ImageIcon(getClass().getResource("/img/Contor.png")));
    }//GEN-LAST:event_jPanelContornoMousePressed

    private void jPanelMefMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelMefMouseEntered
        setColor(jPanelMef);
    }//GEN-LAST:event_jPanelMefMouseEntered

    private void jPanelMefMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelMefMouseExited
        resetColor(jPanelMef);
    }//GEN-LAST:event_jPanelMefMouseExited

    private void jPanelMefMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelMefMousePressed
        jPanelBackDash.setVisible(false);
        jPanelBackMef.setVisible(true);
        jLabelModelImg.setIcon(new ImageIcon(getClass().getResource("/img/modeloa.png")));
        jLabelImgMef.setIcon(new ImageIcon(getClass().getResource(stepMef.get(0))));
        jLabelMefTit.setText(stepTit.get(0));
        jLabelMefDesc.setText(stepDesc.get(0));
    }//GEN-LAST:event_jPanelMefMousePressed

    private void jButtonStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonStartActionPerformed
        
    }//GEN-LAST:event_jButtonStartActionPerformed

    private void jButtonPrevDomMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonPrevDomMousePressed
        clickCount--;
        int i = clickCount;
        System.out.println(clickCount);
        if (i <= 0) {
            clickCount=nameDom.size();            
        }
        jLabelTiImgDom.setText(nameDom.get(i));
        jLabelImgDom.setIcon(new ImageIcon(getClass().getResource(nameDomImg.get(i))));     
    }//GEN-LAST:event_jButtonPrevDomMousePressed

    private void jButtonNextDomMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonNextDomMousePressed
        clickCount++;
        int i = clickCount;
        System.out.println(clickCount);
        if (i >= nameDom.size()-1) {
            clickCount=-1;                         
        }
        jLabelTiImgDom.setText(nameDom.get(i));
        jLabelImgDom.setIcon(new ImageIcon(getClass().getResource(nameDomImg.get(i))));
       
    }//GEN-LAST:event_jButtonNextDomMousePressed

    private void jButtonReturnDomMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnDomMousePressed
        jPanelBackDominio.setVisible(false);
        jPanelBackDash.setVisible(true);        
    }//GEN-LAST:event_jButtonReturnDomMousePressed

    private void jButtonPrevDomMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonPrevDomMouseEntered
        setColorRed(jButtonPrevDom);
    }//GEN-LAST:event_jButtonPrevDomMouseEntered

    private void jButtonPrevDomMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonPrevDomMouseExited
        resetColorRed(jButtonPrevDom);
    }//GEN-LAST:event_jButtonPrevDomMouseExited

    private void jButtonNextDomMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonNextDomMouseEntered
        setColorRed(jButtonNextDom);
    }//GEN-LAST:event_jButtonNextDomMouseEntered

    private void jButtonNextDomMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonNextDomMouseExited
        resetColorRed(jButtonNextDom);
    }//GEN-LAST:event_jButtonNextDomMouseExited

    private void jButtonReturnDomMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnDomMouseEntered
        setColorRed(jButtonReturnDom);
    }//GEN-LAST:event_jButtonReturnDomMouseEntered

    private void jButtonReturnDomMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnDomMouseExited
        resetColorRed(jButtonReturnDom);
    }//GEN-LAST:event_jButtonReturnDomMouseExited

    private void jButtonNextDomMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonNextDomMouseClicked
        
    }//GEN-LAST:event_jButtonNextDomMouseClicked

    private void jButtonPrevMeshMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonPrevMeshMouseEntered
        setColorRed(jButtonPrevMesh);
    }//GEN-LAST:event_jButtonPrevMeshMouseEntered

    private void jButtonPrevMeshMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonPrevMeshMouseExited
        resetColorRed(jButtonPrevMesh);
    }//GEN-LAST:event_jButtonPrevMeshMouseExited

    private void jButtonPrevMeshMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonPrevMeshMousePressed
        clickCountMesh--;
        int i = clickCountMesh;
        System.out.println(clickCountMesh);
        if (i <= 0) {
            clickCountMesh=nameMesh.size();                         
        }
        jLabelTiImgMesh.setText(nameMesh.get(i));
        jLabelImgMesh.setIcon(new ImageIcon(getClass().getResource(nameMeshImg.get(i))));
    }//GEN-LAST:event_jButtonPrevMeshMousePressed

    private void jButtonNextMeshMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonNextMeshMouseClicked
        
    }//GEN-LAST:event_jButtonNextMeshMouseClicked

    private void jButtonNextMeshMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonNextMeshMouseEntered
        setColorRed(jButtonNextMesh);
    }//GEN-LAST:event_jButtonNextMeshMouseEntered

    private void jButtonNextMeshMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonNextMeshMouseExited
        resetColorRed(jButtonNextMesh);
    }//GEN-LAST:event_jButtonNextMeshMouseExited

    private void jButtonNextMeshMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonNextMeshMousePressed
        clickCountMesh++;
        int i = clickCountMesh;
        System.out.println(clickCountMesh);
        if (i == nameMesh.size()-1) {
            clickCountMesh=-1;                         
        }
        jLabelTiImgMesh.setText(nameMesh.get(i));
        jLabelImgMesh.setIcon(new ImageIcon(getClass().getResource(nameMeshImg.get(i))));
    }//GEN-LAST:event_jButtonNextMeshMousePressed

    private void jButtonReturnMeshMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnMeshMouseEntered
        setColorRed(jButtonReturnMesh);
    }//GEN-LAST:event_jButtonReturnMeshMouseEntered

    private void jButtonReturnMeshMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnMeshMouseExited
        resetColorRed(jButtonReturnMesh);
    }//GEN-LAST:event_jButtonReturnMeshMouseExited

    private void jButtonReturnMeshMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnMeshMousePressed
        jPanelBackMesh.setVisible(false);
        jPanelBackDash.setVisible(true); 
    }//GEN-LAST:event_jButtonReturnMeshMousePressed

    private void jButtonReturnMefMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnMefMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonReturnMefMouseEntered

    private void jButtonReturnMefMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnMefMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonReturnMefMouseExited

    private void jButtonReturnMefMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnMefMousePressed
        jPanelBackMef.setVisible(false);
        jPanelBackDash.setVisible(true);
    }//GEN-LAST:event_jButtonReturnMefMousePressed

    private void jPanelButtonModelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelButtonModelMousePressed
        jPanelSubMef.setVisible(false);
        jPanelModel.setVisible(true);
//        jLabelModelImg.setIcon(n);
    }//GEN-LAST:event_jPanelButtonModelMousePressed

    private void jPanelButtonMefMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelButtonMefMousePressed
        jPanelModel.setVisible(false);
        jPanelSubMef.setVisible(true);
    }//GEN-LAST:event_jPanelButtonMefMousePressed

    private void jPanelSigMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelSigMousePressed
        clickCount++;
        int i = clickCount;
        System.out.println(clickCount);
        if (i >= stepTit.size()-1) {
            clickCount=-1;                         
        }
        jLabelMefTit.setText(stepTit.get(i));
        jLabelMefDesc.setText(stepDesc.get(i));
        jLabelImgMef.setIcon(new ImageIcon(getClass().getResource(stepMef.get(i))));
    }//GEN-LAST:event_jPanelSigMousePressed

    private void jPanelAntMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelAntMousePressed
        clickCount--;
        int i = clickCount;
        System.out.println(clickCount);
        if (i >= 0) {
            clickCount= stepTit.size(); 
        }
        jLabelMefTit.setText(stepTit.get(i));
        jLabelMefDesc.setText(stepDesc.get(i));
        jLabelImgMef.setIcon(new ImageIcon(getClass().getResource(stepMef.get(i))));
    }//GEN-LAST:event_jPanelAntMousePressed

    private void jPanelTableConecMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelTableConecMousePressed
        jLabelImgMesh.setIcon(new ImageIcon(getClass().getResource("/img/tabla.png")));
    }//GEN-LAST:event_jPanelTableConecMousePressed

    private void jButtonReturnContornoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnContornoMouseEntered
        setColorRed(jButtonReturnContorno);
    }//GEN-LAST:event_jButtonReturnContornoMouseEntered

    private void jButtonReturnContornoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnContornoMouseExited
        resetColorRed(jButtonReturnContorno);
    }//GEN-LAST:event_jButtonReturnContornoMouseExited

    private void jButtonReturnContornoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnContornoMousePressed
        jPanelBackContorno.setVisible(false);
        jPanelBackDash.setVisible(true);
    }//GEN-LAST:event_jButtonReturnContornoMousePressed

    private void jButtonReturnAssamMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnAssamMouseEntered
        setColorRed(jButtonReturnAssam);
    }//GEN-LAST:event_jButtonReturnAssamMouseEntered

    private void jButtonReturnAssamMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnAssamMouseExited
        resetColorRed(jButtonReturnAssam);
    }//GEN-LAST:event_jButtonReturnAssamMouseExited

    private void jButtonReturnAssamMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonReturnAssamMousePressed
        jPanelBackAssambly.setVisible(false);
        jPanelBackDash.setVisible(true);
    }//GEN-LAST:event_jButtonReturnAssamMousePressed

    private void jPanelsiMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelsiMousePressed
        jLabelAns.setText("<html>Correcto, me haras a llorar ;0</html>");
    }//GEN-LAST:event_jPanelsiMousePressed

    private void jPanelnoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelnoMousePressed
        jLabelAns.setText("<html>Debo explicar mejor, no fuiste tu, fui yo xD.</html>");
    }//GEN-LAST:event_jPanelnoMousePressed

    public void setColor(JPanel panel){
        panel.setBackground(new java.awt.Color(139, 190, 196));
    }
//    97575e
    public void setColorRed(JPanel panel){
        panel.setBackground(new java.awt.Color(151, 87, 94));
    }
    
    public void resetColor(JPanel panel){
        panel.setBackground(new java.awt.Color(127, 183, 190));
    }
    public void resetColorRed(JPanel panel){
        panel.setBackground(new java.awt.Color(107, 15, 26));
    }
    
    public void getIcon() {
        icon  = new ImageIcon(getClass().getResource("/img/di.png"));
        setIconImage(icon.getImage());
    }
    
    public void getIconDom(int index){        
        icon = new ImageIcon(getClass().getResource(nameDomImg.get(index)));        
    }

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Challenge().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JPanel jButtonNextDom;
    public javax.swing.JPanel jButtonNextMesh;
    private javax.swing.JPanel jButtonPrevDom;
    private javax.swing.JPanel jButtonPrevMesh;
    private javax.swing.JPanel jButtonReturnAssam;
    private javax.swing.JPanel jButtonReturnContorno;
    private javax.swing.JPanel jButtonReturnDom;
    private javax.swing.JPanel jButtonReturnMef;
    private javax.swing.JPanel jButtonReturnMesh;
    private javax.swing.JButton jButtonStart;
    private javax.swing.JLabel jIImgAssambly;
    private javax.swing.JLabel jIImgContorno;
    private javax.swing.JLabel jIImgDominio;
    private javax.swing.JLabel jIImgMef;
    private javax.swing.JLabel jIImgMesh;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelAns;
    private javax.swing.JLabel jLabelAssamDesc;
    private javax.swing.JLabel jLabelAssambly;
    private javax.swing.JLabel jLabelContorno;
    private javax.swing.JLabel jLabelDomini;
    private javax.swing.JLabel jLabelDri;
    private javax.swing.JLabel jLabelImgAssam;
    private javax.swing.JLabel jLabelImgAssamTa;
    private javax.swing.JLabel jLabelImgContor;
    private javax.swing.JLabel jLabelImgDom;
    private javax.swing.JLabel jLabelImgMef;
    private javax.swing.JLabel jLabelImgMesh;
    private javax.swing.JLabel jLabelMef;
    private javax.swing.JLabel jLabelMefDesc;
    private javax.swing.JLabel jLabelMefTit;
    private javax.swing.JLabel jLabelMesh;
    private javax.swing.JLabel jLabelModelImg;
    private javax.swing.JLabel jLabelNeu;
    private javax.swing.JLabel jLabelSubT;
    private javax.swing.JLabel jLabelTAssambly;
    private javax.swing.JLabel jLabelTContorno;
    private javax.swing.JLabel jLabelTDom;
    private javax.swing.JLabel jLabelTMef;
    private javax.swing.JLabel jLabelTMesh;
    private javax.swing.JLabel jLabelText;
    private javax.swing.JLabel jLabelTiImgDom;
    private javax.swing.JLabel jLabelTiImgMesh;
    private javax.swing.JLabel jLabelTitle;
    private javax.swing.JLabel jLabelTitleDash;
    private javax.swing.JLabel jLabelTxtDom;
    private javax.swing.JLabel jLabelTxtMesh;
    private javax.swing.JLabel jLabelno;
    private javax.swing.JLabel jLabelsi;
    private javax.swing.JPanel jPanelAnt;
    private javax.swing.JPanel jPanelAssambly;
    private javax.swing.JPanel jPanelBackAssambly;
    private javax.swing.JPanel jPanelBackAuth;
    private javax.swing.JPanel jPanelBackContorno;
    private javax.swing.JPanel jPanelBackDash;
    private javax.swing.JPanel jPanelBackDominio;
    private javax.swing.JPanel jPanelBackMef;
    private javax.swing.JPanel jPanelBackMesh;
    private javax.swing.JPanel jPanelButtonMef;
    private javax.swing.JPanel jPanelButtonModel;
    private javax.swing.JPanel jPanelContorno;
    private javax.swing.JPanel jPanelDominio;
    private javax.swing.JPanel jPanelFront;
    private javax.swing.JPanel jPanelFrontDash;
    private javax.swing.JPanel jPanelMef;
    private javax.swing.JPanel jPanelMesh;
    private javax.swing.JPanel jPanelModel;
    private javax.swing.JPanel jPanelSig;
    private javax.swing.JPanel jPanelSubMef;
    private javax.swing.JPanel jPanelSubMefBack;
    private javax.swing.JPanel jPanelTAssambly;
    private javax.swing.JPanel jPanelTContorno;
    private javax.swing.JPanel jPanelTDom;
    private javax.swing.JPanel jPanelTMef;
    private javax.swing.JPanel jPanelTMesh;
    private javax.swing.JPanel jPanelTableConec;
    private javax.swing.JPanel jPanelno;
    private javax.swing.JPanel jPanelsi;
    private javax.swing.JTextField jTextFieldName;
    // End of variables declaration//GEN-END:variables
}
